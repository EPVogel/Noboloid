from icecube import dataclasses,dataio
from icecube import gulliver
from icecube import lilliput
from icecube.lilliput import scipymin
from icecube import spline_reco
import string
from icecube.photonics_service import I3PhotoSplineService
from icecube.icetray import I3Units
import numpy as n
import healpy as h
import sys
import time
import matplotlib.pyplot as plt
import scipy
import scipy.stats, scipy.special, scipy.interpolate

import argparse
import time as t
import numpy as np

########################################################################
#Helper routines
########################################################################

def makeEventllh(seed_name,PulsesName,EnergyEstimators,configuration,mcTrue_name):
    eventllh = spline_reco.I3SplineRecoLikelihood()
    # disable all modifications
    NoiseModel = "none"
    PreJitter = 4
    PostJitter = 0
    KSConfidenceLevel = 0
    EnergyDependentJitter = False
    EnergyDependentMPE = False
    # enable modifications depending on configuration
    if not configuration == "default": 
        KSConfidenceLevel = 5 
        EnergyDependentMPE = True
    if configuration == "recommended" or configuration == "max":
        PreJitter = 2
        PostJitter = 2
        EnergyDependentJitter = True
    if configuration == "max":
        NoiseModel = "SRT"
    

    BareMuTimingSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfBareMu_mie_prob_z20a10_V2.fits'
    BareMuAmplitudeSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfBareMu_mie_abs_z20a10_V2.fits'
    StochTimingSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfHighEStoch_mie_prob_z20a10.fits'
    StochAmplitudeSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfHighEStoch_mie_abs_z20a10.fits'

    BareMuSplineName = "BareMuSplineJitter" + str(PreJitter) + BareMuAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + BareMuTimingSpline.translate(string.maketrans("",""), string.punctuation)
    StochSplineName = "StochMuSplineJitter" + str(PreJitter) + StochAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + StochTimingSpline.translate(string.maketrans("",""), string.punctuation)
    NoiseSplineName = "NoiseSpline" + BareMuAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + BareMuTimingSpline.translate(string.maketrans("",""), string.punctuation)

    bare_mu_spline = I3PhotoSplineService(BareMuAmplitudeSpline, BareMuTimingSpline, PreJitter)    
    stoch_spline =  I3PhotoSplineService(StochAmplitudeSpline, StochTimingSpline,PreJitter)
    noise_spline = I3PhotoSplineService(BareMuAmplitudeSpline, BareMuTimingSpline, 1000)

    #Setting the parameters of the spline-reconstruction
    eventllh.PhotonicsService = bare_mu_spline
    eventllh.PhotonicsServiceStochastics = stoch_spline
    eventllh.PhotonicsServiceRandomNoise = noise_spline
    eventllh.ModelStochastics = False
    eventllh.NoiseModel = NoiseModel
    eventllh.Pulses = PulsesName
    #eventllh.E_Estimators=EnergyEstimators #<--- THIS DOES NOT WORK YET
    eventllh.Likelihood = "MPE"
    eventllh.NoiseRate = 10*I3Units.hertz
    eventllh.PreJitter = 0
    eventllh.PostJitter = PostJitter
    eventllh.KSConfidenceLevel = KSConfidenceLevel
    eventllh.ChargeCalcStep = 0
    eventllh.CutMode = "late"
    eventllh.EnergyDependentJitter = EnergyDependentJitter
    eventllh.EnergyDependentMPE = EnergyDependentMPE
    #eventllh.E_estimator_names = [EnergyEstimators]
    eventllh.E_Estimators=[EnergyEstimators] #<--- THIS DOES NOT WORK YET
    #eventllh.E_Estimators="SplineMPEICTruncatedEnergySPICEMie_AllDOMS_Muon"#<--- THIS DOES NOT WORK YET apparently this should be a list
    return eventllh

def makeGridFitterParametrizations():
    #Medium fitter for the true minimum
    para_m = lilliput.I3SimpleParametrization("simple_para_medium")                
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_X, 1.*I3Units.m, True )
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Y, 1.*I3Units.m, True )
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Z, 1.*I3Units.m, True )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0, False )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0, True ) 
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0.03 * I3Units.radian, True )
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0.03 * I3Units.radian, True ) 

    #Medium fitter for all predefined points
    para_m_red = lilliput.I3SimpleParametrization("simple_para_medium_red")                
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_X, 1.*I3Units.m, True )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Y, 1.*I3Units.m, True )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Z, 1.*I3Units.m, True )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0, False )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0, True ) 
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0, False )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0, False ) 
    
    return {"min" : para_m,"grid" :para_m_red}
    
def makeFitterCores(eventllh,parametrizations,mini):
    #define the three fitter corse (for each fineness one)
    fitterCore_m = gulliver.I3Gulliver("fitter_m",eventllh,parametrizations["min"],mini)
    fitterCore_m_red = gulliver.I3Gulliver("fitter_mred",eventllh,parametrizations["grid"],mini)
    return {"min" : fitterCore_m,"grid" : fitterCore_m_red}
