########################################################################
#
#Please note that this file will have to be altered as it still uses 
#hard coded links to certain modules. I am not sure wether the 'cvmfs' 
#links for the diffent splines pose any problems for other users or not.
#If so these links have to be altered to your local copy.
#Also please check the code for any disturbing artifacts...
#
########################################################################



from icecube import dataclasses,dataio
from icecube import gulliver
from icecube import lilliput
from icecube.lilliput import scipymin
from icecube import spline_reco
import string
from icecube.photonics_service import I3PhotoSplineService
from icecube.icetray import I3Units
import numpy as n
import healpy as h
import sys
import time
import matplotlib.pyplot as plt
import scipy.stats, scipy.special, scipy.interpolate

import argparse
import time as t
import numpy as np
  

#######################################################################

def bestfit(fit_results):
    min_llh = (n.inf,-1)
    for nfr,fr in enumerate(fit_results):
        if fr.logl < min_llh[0] or min_llh[1] == -1:
            min_llh = (fr.logl,nfr)
    return min_llh[1]

#SUBMETHODS FOR THE FITTERS


def fit_llh(seeds,fitterCores,mcTrue = None,verbose=False):
    """Fit the best direction.
    
    nside (int) obsolet
    seeds, seeds for the fitter
    pix_radius (float) obsolet
    fitterCores (list), list of fitter modules (two are required)
    """
    
    fitterCore = fitterCores[0]
    fitterCoreRed = fitterCores[1]
    if not isinstance(seeds,list):
        seeds = [seeds]

    fit_results = []
    particles = []
    
    #Run Fitter over all seeds
    for nseed,seed in enumerate(seeds):
        s = t.time()
        #build an i3particle seed
        this_seed = dataclasses.I3Particle(seed)

        #set the event hypothesis
        hypo = gulliver.I3EventHypothesis(this_seed)
        #run the fitter
        fit_results.append(fitterCore.Fit(frame,hypo))    
        particles.append(hypo.particle)
        e = t.time()
        if verbose:
            print("Finished full 5-parameter fit. %f s"%(e-s))
    if mcTrue != None:
        s = t.time()
        #if the mcTruth is given compute the llh values at the MCTruth position
        this_seed = dataclasses.I3Particle(seeds[0])
        #direc = dataclasses.I3Direction(mcTrue.dir["zenith"],mcTrue.dir["azimuth"])
        direc = dataclasses.I3Direction(mcTrue.dir.zenith,mcTrue.dir.azimuth)
        this_seed.dir = direc
        
        #Carry out the the logllh computation
        hypo = gulliver.I3EventHypothesis(this_seed)
        mcTrueResult = fitterCoreRed.Fit(frame,hypo)    
        mcTrueParticle = hypo.particle
        e = t.time()
        if verbose:
            print("Finished full 3-parameter fit. %f s"%(e-s))
        return fit_results,particles, mcTrueResult,mcTrueParticle
        
    else:
        return fit_results,particles
    
def scan_grid(directions,oSeed,fitterCores, verbose = False):
    """Scan a certain grid of parameters for their llh values."""
    seed = oSeed
    fitResults = []
    particles = []
    fitterCoreRed = fitterCores[1]
    
    for (zenith,azimuth) in directions:
        s = t.time()
        #build a I3 direciton
        #only allow a single seed for now
        #modify the seed so that it its 
        direc = dataclasses.I3Direction(zenith,azimuth)
        this_seed = dataclasses.I3Particle(seed)
        this_seed.dir = direc
        
        #Carry out the the logllh computation
        hypo = gulliver.I3EventHypothesis(this_seed)
        fitResults.append(fitterCoreRed.Fit(frame,hypo))    
        particles.append(hypo.particle)
        e = t.time()
        if verbose:
            print("Finished full 3-parameter fit. %f s"%(e-s))
            
    return fitResults, particles
           

def calc_sigma(llhmap,llhmin,ndof):
    deltaLLH = 2*(llhmap-min_llhs)
    pval = scipy.stats.chi2.cdf(deltaLLH,ndof)   
    sigma = scipy.special.erfinv(pval)
    
    return sigma
    
def pixels_in_radius(dir,radius,nside):    
    scan_pix = h.query_disc(nside=nside,vec=(-dir.x,-dir.y,-dir.z),radius=radius)
    return len(scan_pix)

def uniqueId(runId,eventId,maxEventId = 1234567):
    return maxEventId * runId + eventId

#FITTER HANDLER ETC.

def computeInitialDirection(frame, seedName, PulsesName, EnergyEstimators,configuration):
    """This function computes the initial guess direction. """
    ####################################################################
    #Settings
    ####################################################################
    
    ####################################################################
    #Retrieve arguments                                                #
    ####################################################################

    if len(EnergyEstimators)==0 and not configuration == "default":
        raise RuntimeError("If configuration is not 'default' at least one EnergyEstimator has to be set.")
    
    #vertex steps used for altering the seed direction
    #Q: What is the unit of the deviation? Supposedly meters???
    vertex_steps = [1,3,10]
 
    ######################################
    ##Define Spline LLH as in spline_reco traysegment
    ######################################
    eventllh = spline_reco.I3SplineRecoLikelihood()
    # disable all modifications
    NoiseModel = "none"
    PreJitter = 4
    PostJitter = 0
    KSConfidenceLevel = 0
    EnergyDependentJitter = False
    EnergyDependentMPE = False
    # enable modifications depending on configuration
    if not configuration == "default": 
        KSConfidenceLevel = 5 
        EnergyDependentMPE = True
    if configuration == "recommended" or configuration == "max":
        PreJitter = 2
        PostJitter = 2
        EnergyDependentJitter = True
    if configuration == "max":
        NoiseModel = "SRT"
    

    BareMuTimingSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfBareMu_mie_prob_z20a10_V2.fits'
    BareMuAmplitudeSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfBareMu_mie_abs_z20a10_V2.fits'
    StochTimingSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfHighEStoch_mie_prob_z20a10.fits'
    StochAmplitudeSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfHighEStoch_mie_abs_z20a10.fits'

    BareMuSplineName = "BareMuSplineJitter" + str(PreJitter) + BareMuAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + BareMuTimingSpline.translate(string.maketrans("",""), string.punctuation)
    StochSplineName = "StochMuSplineJitter" + str(PreJitter) + StochAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + StochTimingSpline.translate(string.maketrans("",""), string.punctuation)
    NoiseSplineName = "NoiseSpline" + BareMuAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + BareMuTimingSpline.translate(string.maketrans("",""), string.punctuation)

    bare_mu_spline = I3PhotoSplineService(BareMuAmplitudeSpline, BareMuTimingSpline, PreJitter)    
    stoch_spline =  I3PhotoSplineService(StochAmplitudeSpline, StochTimingSpline,PreJitter)
    noise_spline = I3PhotoSplineService(BareMuAmplitudeSpline, BareMuTimingSpline, 1000)

    #Setting the parameters of the spline-reconstruction
    eventllh.PhotonicsService = bare_mu_spline
    eventllh.PhotonicsServiceStochastics = stoch_spline
    eventllh.PhotonicsServiceRandomNoise = noise_spline
    eventllh.ModelStochastics = False
    eventllh.NoiseModel = NoiseModel
    eventllh.Pulses = PulsesName
    eventllh.E_Estimators=EnergyEstimators
    eventllh.Likelihood = "MPE"
    eventllh.NoiseRate = 10*I3Units.hertz
    eventllh.PreJitter = 0
    eventllh.PostJitter = PostJitter
    eventllh.KSConfidenceLevel = KSConfidenceLevel
    eventllh.ChargeCalcStep = 0
    eventllh.CutMode = "late"
    eventllh.EnergyDependentJitter = EnergyDependentJitter
    eventllh.EnergyDependentMPE = EnergyDependentMPE

    #####################################
    ##Define Parametrizations############
    #####################################

    #Medium fitter for the true minimum
    para_m = lilliput.I3SimpleParametrization("simple_para_medium")                
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_X, 1.*I3Units.m, True )
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Y, 1.*I3Units.m, True )
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Z, 1.*I3Units.m, True )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0, False )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0, True ) 
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0.03 * I3Units.radian, True )
    para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0.03 * I3Units.radian, True ) 

    #Medium fitter for all predefined points
    para_m_red = lilliput.I3SimpleParametrization("simple_para_medium_red")                
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_X, 1.*I3Units.m, True )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Y, 1.*I3Units.m, True )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Z, 1.*I3Units.m, True )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0, False )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0, True ) 
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0, False )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0, False ) 


    #define the minimizer to use
    mini =scipymin.SciPyMinimizer(name="scipy_simplex_f", method='Nelder-Mead', tolerance=args.tolerance,max_iterations=1000)
    
    #define the three fitter corse (for each fineness one)
    fitterCore_m =gulliver.I3Gulliver("fitter_m",eventllh,para_m,mini)
    fitterCore_m_red =gulliver.I3Gulliver("fitter_mred",eventllh,para_m_red,mini)
    
    
    #add a common identifier
    uID = uniqueId(frame["I3EventHeader"].run_id,frame["I3EventHeader"].event_id)
    
    
    
    #retieve the seed and if possible the MC Truth
    seed = frame[seed_name]
    mcTrue = frame[mcTrue_name]
    
    seeds = [seed]
    #setting an original seed
    oSeed = seed
    #Altering the initial seed direction
    #note this does not change the energy or anything and will be useless for the scanning process!
    for vs in vertex_steps:
        newP= dataclasses.I3Particle(seed)
        newP.pos.x +=vs
        seeds.append(newP)
        
        newP= dataclasses.I3Particle(seed)
        newP.pos.x -=vs
        seeds.append(newP)
        
        ##
        
        newP= dataclasses.I3Particle(seed)
        newP.pos.y +=vs
        seeds.append(newP)
        
        newP= dataclasses.I3Particle(seed)
        newP.pos.y -=vs
        seeds.append(newP)
        
        ##
        newP= dataclasses.I3Particle(seed)
        newP.pos.z +=vs
        seeds.append(newP)
        
        newP= dataclasses.I3Particle(seed)
        newP.pos.z -=vs
        seeds.append(newP)
        
    #run the fit algorithm
    fitterCores = [fitterCore_m,fitterCore_m_red]
    
    # get the particles
    fit_results,particles, mcTrueResult,mcTrueParticle  = fit_llh(seeds,fitterCores,
            verbose=args.verbose, mcTrue = mcTrue)
            
    #reduce it to the best fit
    best_fits = []
    best_particles = []
    best_index = bestfit(fit_results)
    best_fits = [fit_results[best_index]]
    best_particles = [particles[best_index]]
    
    minResult = fit_results[best_index]
    minParticle = particles[best_index]
    
    return {"zenith" : minParticle.dir.zenith , "azimuth" : minParticle.dir.azimuth}

    



def computeGridLikelihoods(frame,directions,seedName,PulsesName,EnergyEstimators,configuration):
    """This function is intended to compute the grid likelihoods.
    
    frame : I3 physics frame
    directions : directions of the circular evaluation grid
    """

    if len(EnergyEstimators)==0 and not configuration == "default":
        raise RuntimeError("If configuration is not 'default' at least one EnergyEstimator has to be set.")
    
    #vertex steps used for altering the seed direction
    #Q: What is the unit of the deviation? Supposedly meters???
    vertex_steps = [1,3,10]
 
    ######################################
    ##Define Spline LLH as in spline_reco traysegment
    ######################################
    eventllh = spline_reco.I3SplineRecoLikelihood()
    # disable all modifications
    NoiseModel = "none"
    PreJitter = 4
    PostJitter = 0
    KSConfidenceLevel = 0
    EnergyDependentJitter = False
    EnergyDependentMPE = False
    # enable modifications depending on configuration
    if not configuration == "default": 
        KSConfidenceLevel = 5 
        EnergyDependentMPE = True
    if configuration == "recommended" or configuration == "max":
        PreJitter = 2
        PostJitter = 2
        EnergyDependentJitter = True
    if configuration == "max":
        NoiseModel = "SRT"
    

    BareMuTimingSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfBareMu_mie_prob_z20a10_V2.fits'
    BareMuAmplitudeSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfBareMu_mie_abs_z20a10_V2.fits'
    StochTimingSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfHighEStoch_mie_prob_z20a10.fits'
    StochAmplitudeSpline = '/cvmfs/icecube.opensciencegrid.org/data/photon-tables/splines/InfHighEStoch_mie_abs_z20a10.fits'

    BareMuSplineName = "BareMuSplineJitter" + str(PreJitter) + BareMuAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + BareMuTimingSpline.translate(string.maketrans("",""), string.punctuation)
    StochSplineName = "StochMuSplineJitter" + str(PreJitter) + StochAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + StochTimingSpline.translate(string.maketrans("",""), string.punctuation)
    NoiseSplineName = "NoiseSpline" + BareMuAmplitudeSpline.translate(string.maketrans("",""), string.punctuation) + BareMuTimingSpline.translate(string.maketrans("",""), string.punctuation)

    bare_mu_spline = I3PhotoSplineService(BareMuAmplitudeSpline, BareMuTimingSpline, PreJitter)    
    stoch_spline =  I3PhotoSplineService(StochAmplitudeSpline, StochTimingSpline,PreJitter)
    noise_spline = I3PhotoSplineService(BareMuAmplitudeSpline, BareMuTimingSpline, 1000)

    #Setting the parameters of the spline-reconstruction
    eventllh.PhotonicsService = bare_mu_spline
    eventllh.PhotonicsServiceStochastics = stoch_spline
    eventllh.PhotonicsServiceRandomNoise = noise_spline
    eventllh.ModelStochastics = False
    eventllh.NoiseModel = NoiseModel
    eventllh.Pulses = PulsesName
    eventllh.E_Estimators=EnergyEstimators
    eventllh.Likelihood = "MPE"
    eventllh.NoiseRate = 10*I3Units.hertz
    eventllh.PreJitter = 0
    eventllh.PostJitter = PostJitter
    eventllh.KSConfidenceLevel = KSConfidenceLevel
    eventllh.ChargeCalcStep = 0
    eventllh.CutMode = "late"
    eventllh.EnergyDependentJitter = EnergyDependentJitter
    eventllh.EnergyDependentMPE = EnergyDependentMPE

    #####################################
    ##Define Parametrizations############
    #####################################

    #Medium fitter for all predefined points
    para_m_red = lilliput.I3SimpleParametrization("simple_para_medium_red")                
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_X, 1.*I3Units.m, True )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Y, 1.*I3Units.m, True )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Z, 1.*I3Units.m, True )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0, False )
    #para_m.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0, True ) 
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Zen, 0, False )
    para_m_red.SetStep( lilliput.I3SimpleParametrization.PAR_Azi, 0, False ) 


    #define the minimizer to use
    mini =scipymin.SciPyMinimizer(name="scipy_simplex_f", method='Nelder-Mead', tolerance=args.tolerance,max_iterations=1000)
    
    #define the three fitter corse (for each fineness one)
    fitterCore_m_red =gulliver.I3Gulliver("fitter_mred",eventllh,para_m_red,mini)
    
    #retieve the seed and if possible the MC Truth
    seed = frame[seed_name]
    
    #run the fit algorithm
    fitterCores = [fitterCore_m_red]
    
    gridFitResults, gridParticles = scan_grid(directions,oSeed,fitterCores, verbose = True)

    #retrieve the information for all the points
    gridPoints = {"llh" : [],"azimuth" : [], "zenith" : []}
    for gridResult, gridParticle in zip(gridFitResults, gridParticles):
        gridPoints["llh"].append(gridResult.logl)
        gridPoints["azimuth"].append(gridParticle.dir.azimuth)
        gridPoints["zenith"].append(gridParticle.dir.zenith)
    
    return gridPoints
