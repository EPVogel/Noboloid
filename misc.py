import pickle
import os
import numpy as np
import scipy
from scipy import interpolate
import pylab as p
import scipy.stats as st
from scipy.stats.kde import gaussian_kde
import copy
import time as t
import sys
class NParDataLookup(object):
    def __init__(self,data,*parameters,**kwargs):
        """Create the lookup. 
        
        P
        """
        
        identifiers = kwargs["identifiers"] if "identifiers" in kwargs else ["%i"%i for i in range(len(parameters))]
        
    
        #Numpy array cast if necessary
        data = data if isinstance(data,np.ndarray) else np.array(data)
        parameters = np.array(parameters)

        
        #Get the required shape
        dimensions = np.array([len(elem) for elem in parameters])
        if not np.array_equal(dimensions,np.array(data.shape)):
        #Validate the shape of the data and the parameters
            raise ValueError("Shapes to not match!")
        if not len(parameters) == len(identifiers):
            raise ValueError("Identifiers do not match shape of parameters.")
        
        #Find the boundaries of each element
        limits = np.array([(np.min(elem.flatten()),np.max(elem.flatten())) for elem in parameters])
        
        #now make the limits all show into the same direction
        limits_ = np.array([elem - elem[0] for elem in limits])
        
        #determine any possible offsets
        offsets = np.array([-elem[0] for elem in limits])
        
        self._limits = limits_
        self._offsets = offsets
        self._sizes = dimensions
        self._data = data
        self._identifiers = identifiers
        """
        print("Begin status information")
        print(limits)
        print(self._limits)
        print(self._offsets)
        print(self._sizes)
        print("End of status informatoion")
        """
        
    def valToIndex(self,dValue):
        """Compute the index based on the value."""
        
        dValue = dValue if isinstance(dValue, np.ndarray) else np.array(dValue)
        if len(dValue) != len(self._sizes):
            raise ValueError("Dimensionalty not matching.")
            
        #apply the offset to the value
        dValue = dValue + self._offsets
        cleanedVals = [ val if ( self._limits[iV][0] < val and self._limits[iV][1] > val) else self._limits[iV][0] if self._limits[iV][0] > val else  self._limits[iV][1] for iV,val in enumerate(dValue)]
        indexList = [round((cleanedVals[iV]/self._limits[iV][1] * (self._sizes[iV] -1.0))) for iV in range(3)]
        
        return np.array(indexList).astype(np.int32)
        
    def query(self,dValue,getIndices = False):
        """Acess the closest point."""

        indices = self.valToIndex(dValue)
        if getIndices:
            return self._data[list(indices.reshape(len(indices),1))], indices
        else:
            return self._data[list(indices.reshape(len(indices),1))]

    def interpolated(self,dValue,what = "linear"):
        raise NotImplementedError("This has not been implemented yet")
        """Interpolate between the two closest points."""
        dValue = dValue if isinstance(dValue, np.ndarray) else np.array(dValue)
        
        values, indices = self.query(dValue)
        
        #determine bounds
        decisions = np.greater(dValue,values)
        
        newIndices = np.array([ind + 1 if dec else ind - 1  for ind,dec in zip(indices,decisions)]) 
        #check for each value, if dValue is larger or smaller
        
        for ind,newInd in zip(indices,newIndices):
            pass
        
        #linear interpolation
        
        #cubic interpolation will be added later
        
        
        #for all index pairs interpolate
        
    def dump(self, path):
        pickle.dump(self,open(path,"wb"))
        
    @property
    def limits(self):
        return np.array([elem - elem2 for elem,elem2 in zip(self._limits,self._offsets)])

    @property
    def sizes(self):
        return self._sizes
        
    @property
    def identifiers(self):
        return self._identifiers
