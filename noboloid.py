#!/usr/bin/env python
from icecube.icetray import I3ConditionalModule,I3Units,logging
from icecube import gulliver
from icecube import lilliput
from icecube.gulliver import I3Gulliver
from icecube.gulliver import I3SeedService, I3SeedServiceBase
from icecube.gulliver import I3EventLogLikelihood, I3EventLogLikelihoodBase
from icecube.gulliver import I3Minimizer, I3MinimizerBase
from icecube.lilliput import I3SimpleParametrization,scipymin
from icecube.dataclasses import I3Direction,I3Position,I3Particle,I3VectorDouble,I3Double
from icecube.paraboloid import I3ParaboloidFitParams
from .config_util import get_service_by_name_or_by_object
from .gridpoint import gridpoint
from misc import NParDataLookup
#from .fit_paraboloid import fit_paraboloid
from .noboloid_fitters import fit_llh
from .config import makeEventllh,makeGridFitterParametrizations,makeFitterCores
from numpy import cos,sin
import numpy as np
import scipy
import copy
import time as t
import cPickle as pickle

class Noboloid(I3ConditionalModule):
    """
    Noboloid is a python implementation of a non-parametric angular error 
    estimation algorithm (in contrast to paraboloid called Noboloid). The
    algorithm is based on the connection of log-likelihood ratios and contour
    based error estimation.
    """
    def __init__(self,ctx):
        super(Noboloid,self).__init__(ctx)
        #basic settings or should they be moved to Configure
        self.logl_service       = None
        self.seed_service       = None
        self.mini_service       = None
        self.XYZStep            = 10.*I3Units.m
        self.NRings             = 3
        self.NPointsPerRing     = 8
        self.grid               = []
        self.seedName           = "SplineMPEIC"
        self.tolerance          = 0.00001
        self.radius             = 0.5
        self.interpolationPoints= 100
        self.pValBounds         = np.array([0.32,0.10,0.05]) 
        #TODO
        #These should be outsourced
        self.pulses = "TWSRTHVInIcePulsesIC"
        self.e_recos = "SplineMPEICTruncatedEnergySPICEMie_AllDOMS_Muon"
        self.config = "max"
        self.mctrue = "MCMuon"
        self.interpolationMethod = "rbf"
        #The path to the kde which should be stored in a pickled 
        self._kdePath = "/net/scratch_icecube4/user/vogel/Data/MuonGun/KDEs/FullSpaceCoverage/fullCoverage.pkl"
        self.kdePath = "/home/faktum/Data/PhysicsData/fullCoverage.pkl"
        
        self.buildEnvironment(seed_name = self.seedName,
                              PulsesName = self.pulses,
                              EnergyEstimators = self.e_recos,
                              configuration = self.config,
                              mcTrue_name = self.mctrue)
        
    def Configure(self):
        pass
        

    def Geometry(self, frame):
        #do nothing with such a frame
        self.PushFrame(frame)
        
    def Physics(self, frame):
        """How to process a Physics frame. (Main Algorithm).
        
        Basic steps of the algorirthm:
        (1) Compute a grid of points surrounding the central position of
        the reconstructed direction. Use the same code or at least the
        same algorithm as in the paraboloid implementaiton.
        (2) Carry out a likelihood computation for these grid-points.
        (3) Interpolate the grid over a given range using a smoothing
        spline based on radial basis functions provided by the scipy
        module.
        (4) Compute likelihood-ratios (-2 * (llh - llh_Min)) for all
        points of the interpolation grid.
        (5) Translate the likelihood-ratios into PValues using a Lookup-
        table based on Kernel-Density-Estimates of the distributions.
        (6) Compute the PValue of given ratio and link it to an area.
        (7) Store these informations in the frame.
        """
        if frame.Has(self.seedName):
            #1 call the grid method to get a grid of i3particle
            seedParticle = frame[self.seedName]
            
            grid = self.createGrid(seedParticle)
            
            #2 carry out the fit around the grid
            particles, fitResults = self.fitGrid(frame,grid,verbose = True)

            #3 interpolate over a predefined grid
            try:
                XI,YI,ZI,A = self.interpolateGrid(particles,fitResults)
            except np.linalg.LinAlgError:
                print("WARNING: Interpolation could not have been carried out. Use Fallback method.")
                
                #for now just continue with the next frame
                self.PushFrame(frame)
                
            #4 Compute the likelihood ratios
            lambdaI = 2.0 * (ZI - np.min(ZI))
        
            #5 Translate likelihood ratios into Pvalues
            #iterate over the set -> this should be speed up at a later stage
            PVals = []
            
            zenith = seedParticle.dir.zenith *180.0 / np.pi
            energy = frame[self.e_recos].energy
            
            #slower implementation preserving the order
            """
            for row in lambdaI:
                #query works like this self.kde.query([energy,Zenith,lambdaVal])
                #please note that Energy should be provided in GeV and the zenith in degrees
                PVals.append([ (1.0 - self.kde.query([energy,zenith,lambdaVal])[0]) for lambdaVal in row ])
            PVals = np.array(PVals)
            """
            pVals = np.array([1 - self.kde.query([energy,zenith,lambdaVal])[0] for lambdaVal in lambdaI.flatten()])
            
            #6 Compute the PValue over something ratio in the frame.
            ratios = []
            areas = []
            for pBound in self.pValBounds:
                #is the shape information also required?
                ZFiltered = np.ma.masked_greater(pVals,pBound)
                nIn = np.sum(ZFiltered.mask.astype(int))
                ZFiltered.mask = ~ZFiltered.mask
                
                
                #compute the area of the enclosed contour
                
                #s = t.time()
                #ZFiltered = np.ma.masked_greater(pValues,PVal)
                #nIn = np.sum(ZFiltered.mask.astype(int))
                #ZFiltered.mask = ~ZFiltered.mask
                #max be unnecessary if one directly uses the mask
                #ZFiltered_ = ZFiltered.compressed()

                #e = t.time()
                
                #estimatedCoveredArea = len(ZFiltered) * A #estimated covered area
                estimatedCoveredArea = nIn *A
                ratio = pBound / estimatedCoveredArea
                ratios.append(ratio)
                areas.append(estimatedCoveredArea)
            
            #There is a bug here....
            frame.Put("pValueAreaRatios",I3VectorDouble(ratios))
            frame.Put("pValueAreas",I3VectorDouble(areas))
            frame.Put("pValues",I3VectorDouble(self.pValBounds))
            frame.Put("areaElement",I3Double(A))
            #7 Store all these values in the frame use i3 objects for this
            
        else:
            print("Nono")
        self.PushFrame(frame)
        
        
    ####################################################################
    #Set up the environment
    ####################################################################    
        
    def buildEnvironment(self,
                         seed_name,
                         PulsesName,
                         EnergyEstimators,
                         configuration,
                         mcTrue_name):
        """Set up the fit environment. """
        ################################################################
        #THIS WILL BE REMOVED AS SOON AS THE FITTER HAS BEEN IMPLEMENTED
        #AS A SERVICE
        ################################################################
        self.eventLLH = makeEventllh(seed_name,PulsesName,EnergyEstimators,configuration,mcTrue_name)
        self.gridFitterParametrizations = makeGridFitterParametrizations()

        
        #define the minimizer to use in the process
        self.minimizer = scipymin.SciPyMinimizer(name="scipy_simplex_f",
                                                 method='Nelder-Mead',
                                                 tolerance=self.tolerance,
                                                 max_iterations=1000)
        
        #define the fitterCores                                    
        self.fitterCores = makeFitterCores(self.eventLLH,
                                           self.gridFitterParametrizations,
                                           self.minimizer)
        
        #define the Kernel-Density-Estimate one wants to use
        #from misc import NParDataLookup
        test = NParDataLookup("nothing")
        print("Made this thing!")
        #TODO: THIS MUST BE CHANGED GET AWAY FROM THE PICKLE FILES AS THIS CAUSES HORRIBLE SHITNESS OF DOOM AND SADNESS. THES MODULE MUST BE IMPORTED AT THE TOP LEVEL SO THIS M-U-S-T BE CHANGED.
    
        self.kde = pickle.load(open(self.kdePath,"rb"))
        try:
            self.kde = pickle.load(open(self.kdePath,"rb"))
        except:
            print("WUUUUUUUT")
        
    ####################################################################
    #computation methods
    ####################################################################
        
    def createGrid(self,seedParticle):
        """Create a grid of i3particles around a seed particle. """
        grid = []
        for i in range(1,self.NRings +1):
            r = i*self.radius/self.NRings
            for j in range(self.NPointsPerRing):
                azimuth = seedParticle.dir.azimuth  + cos(j*2.*np.pi/self.NPointsPerRing)*self.radius * np.pi /180.0
                zenith = seedParticle.dir.zenith + sin(j*2.*np.pi/self.NPointsPerRing)*self.radius * np.pi /180.0
                nParticle= I3Particle(seedParticle)
                direc = I3Direction(zenith,azimuth)
                nParticle.dir = direc
                grid.append(nParticle)
        return grid
    
    def fitGrid(self,frame,grid, verbose = False):
        """Carry out the fit over the grid. """
        fitResults = []
        particles = []
        fitterCoreRed = self.fitterCores["grid"]
        for particle in grid:
            s = t.time()
            hypo = gulliver.I3EventHypothesis(particle)
            res = fitterCoreRed.Fit(frame,hypo)
            fitResults.append(res)   
            particles.append(hypo.particle)
            e = t.time()
            if verbose:
                print("Finished 3-parameter grid minimization in %f seconds.\nParameters: %s"%((e-s),str(res)))
        
        return particles, fitResults
        
    def interpolateGrid(self,particles,fitResults):
        #build arr
        arr = np.array(zip(*[(particle.dir.zenith,particle.dir.azimuth) for particle in particles]))
        val = np.array([res.logl for res in fitResults])
        
        #the limits will be set automatically to the min max values of the range
        XI,YI,ZI,A = self._carefulInterpolation(arr,val, nPoints = self.interpolationPoints)
        #'A' is the size of the element
        
        return XI,YI,ZI,A
        
        
    ####################################################################
    #additional methods
    ####################################################################
        
        
    def _carefulInterpolation(self,arr, val, nPoints, NaN = 0.0,**kwargs):
        """Carry out a careful interpolation of the given points. 
        
        Careful interpolation will perform a smoothing spline interpolation
        based on radial basis funcitons provided by the scipy.interpolate
        module.
        """ 
        XI,YI,ZI,A = self._interpolateMeshGrid(arr, val,nPoints = nPoints,method = self.interpolationMethod,**kwargs)
        
        #overwrite bad values:
        s = t.time()
        ZI[np.isnan(ZI)] = NaN
        e = t.time()
        #print("Version 1 took %f seconds"%(e-s))

        return XI,YI,ZI,A
        
    def _interpolateMeshGrid(self,arr,Z,**kwargs):
        """Carry out the interpolation based on different sub-methods. """

        nPoints = kwargs.pop("nPoints") if "nPoints" in kwargs else 500
        limits = kwargs.pop("limits") if "limits" in kwargs else [[arr[0].min(),arr[0].max()],[arr[1].min(),arr[1].max()]]
        method = kwargs.pop("method") if "method" in kwargs else "linear"

        
        xi, yi = np.linspace(limits[0][0], limits[0][1], nPoints), np.linspace(limits[1][0], limits[1][1], nPoints)
        A = (xi[1] - xi[0]) * (yi[1] - yi[0])
        xi, yi = np.meshgrid(xi, yi)
        
        print("The limits",limits)
        
        print(xi)
        
        if method == "linear":
            #use the standard 
            zi = scipy.interpolate.griddata((arr[0], arr[1]), Z, (xi, yi), method=method)
        elif method == "rbf":
            #use radial basis functions allow for smoothing
            rbf = scipy.interpolate.Rbf(arr[0], arr[1], Z, epsilon = 2.0, smooth = 0.008)
            zi = rbf(xi, yi)

        return xi,yi,zi,A
        

        
"""    
#OLD FITTER HERE
 ####################################################################
#Retrieve arguments                                                #
####################################################################

seed_name = args.seed
PulsesName = args.pulses
EnergyEstimators = args.e_recos
configuration = args.config
mcTrue_name = args.mctrue
if len(EnergyEstimators)==0 and not configuration == "default":
    raise RuntimeError("If configuration is not 'default' at least one EnergyEstimator has to be set.")

#vertex steps used for altering the seed direction
#Q: What is the unit of the deviation? Supposedly meters???
vertex_steps = [1,3,10]

######################################
##Define Spline LLH as in spline_reco traysegment
######################################


#####################################
##Define Parametrizations############
#####################################

#add a common identifier
uID = uniqueId(frame["I3EventHeader"].run_id,frame["I3EventHeader"].event_id)

#retieve the seed and if possible the MC Truth
seed = frame[seed_name]
mcTrue = frame[mcTrue_name]


    
#run the fit algorithm
fitterCores = [fitterCore_m,fitterCore_m_red]

# get the particles
fit_results,particles, mcTrueResult,mcTrueParticle  = fit_llh(seeds,fitterCores,
        verbose=args.verbose, mcTrue = mcTrue)
        
#reduce it to the best fit
best_fits = []
best_particles = []
best_index = bestfit(fit_results)
best_fits = [fit_results[best_index]]
best_particles = [particles[best_index]]

minResult = fit_results[best_index]
minParticle = particles[best_index]
        
#build a circle of positions around the min
#directions = circularEvaluationPoints(minPos = [minParticle["zenith"],minParticle["azimuth"]],fullOutput = False,maxR = 2.0,nPoints = 8)
directions = circularEvaluationPoints(minPos = [minParticle.dir.zenith,minParticle.dir.azimuth],fullOutput = False,maxR = 2.0 * np.pi / 180.0,nPoints = 8)
#scan the grid of points
print("Calling ScanGrid")
gridFitResults, gridParticles = scan_grid(directions,oSeed,fitterCores, verbose = True)
print("Ending ScanGrid")

#Retrieve the most important data
import cPickle as pickle
pDict = {}
pDict["min"] = {"llh" : minResult.logl,
                "direc" : {"zenith" : minParticle.dir.zenith,"azimuth" : minParticle.dir.azimuth}}
                
pDict["MCTruth"] = {"llh" : mcTrueResult.logl,
                    "direcF" : {"zenith" : mcTrueParticle.dir.zenith, "azimuth" : mcTrueParticle.dir.azimuth},
                    "direcT" : {"zenith" : mcTrue.dir.zenith, "azimuth" : mcTrue.dir.azimuth}}
    
tmpPoints = []
for gridResult, gridParticle in zip(gridFitResults, gridParticles):
    tmpPoints.append({"llh" : gridResult.logl,
                "direc" : {"zenith" : gridParticle.dir.zenith,"azimuth" : gridParticle.dir.azimuth}})
                
pDict["gridPoints"] = tmpPoints
pDict["energy"] = getattr(minParticle,"energy")
pDict["energyT"] = getattr(mcTrue,"energy")

#add a common identifier

pDict["uniqueID"] = uID
pickle.dump(pDict,open(args.outfile,"w"))
e = t.time()
if args.verbose:
    print("Finished execution of one frame took%f seconds"%(e-s))
"""
        
        
#ToDo: What defines the conditional module?
class Noboloid_(I3ConditionalModule):
    """
    Noboloid is a python implementation of a non-parametric angular error 
    estimation algorithm (in contrast to paraboloid called Noboloid). The
    algorithm is based on the connection of log-likelihood ratios and contour
    based error estimation.
    """
    def __init__(self,ctx):
        super(Noboloid,self).__init__(ctx)
        #basic settings or should they be moved to Configure
        self.logl_service     = None
        self.seed_service     = None
        self.mini_service     = None
        self.xyz_step         = 10.*I3Units.m
        self.nrings           = 2
        self.npoints_per_ring = 8
        self.grid             = []
        self.AddParameter("LikelihoodService","Event likelihood service (should be the same as the one you used to get the seed fit).",None)
        self.AddParameter("SeedService","Seed service (delivers the input track for which you want to know the angular uncertainty).",None)
        self.AddParameter("GridPointSeedService","Optional point seed service (optional), used to tweak (e.g. vertex time) seeds on non-central grid points.",None)
        self.AddParameter("MinimizerService","Minimizer service (optional): used to optimize xyz for each direction grid point.",None)
        # TODO: split out grid making stuff into separate class
        self.AddParameter("VertexStepSize","Step size for vertex refit on each grid point (minimizer).",10.*I3Units.m)
        self.AddParameter("NRings","Number of rings around seed direction",2)
        self.AddParameter("NPointsPerRing","Number of grid points per ring",8)
        self.AddParameter("Radius","Radius (half opening angle) of the outer ring",1.*I3Units.degree)
        
    def Configure(self):
        self.logl_service     = get_service_by_name_or_by_object(self,"LikelihoodService",I3EventLogLikelihood,I3EventLogLikelihoodBase,False)
        self.seed_service     = get_service_by_name_or_by_object(self,"SeedService",I3SeedService,I3SeedServiceBase,False)
        self.mini_service     = get_service_by_name_or_by_object(self,"MinimizerService",I3Minimizer,I3MinimizerBase,True)
        self.xyz_step         = float(self.GetParameter("VertexStepSize"))
        self.nrings           = int(self.GetParameter("NRings"))
        self.npoints_per_ring = int(self.GetParameter("NPointsPerRing"))
        self.radius           = float(self.GetParameter("Radius"))
        if self.mini_service:
            self.para_service = I3SimpleParametrization(self.name+"_xyz_par")
            self.para_service.SetStep(I3SimpleParametrization.PAR_X,self.xyz_step,False)
            self.para_service.SetStep(I3SimpleParametrization.PAR_Y,self.xyz_step,False)
            self.para_service.SetStep(I3SimpleParametrization.PAR_Z,self.xyz_step,True)
            self.gulliver     = I3Gulliver(self.name,self.logl_service,self.para_service,self.mini_service)
            logging.log_debug("doing marginalization on grid points",unit=self.name)
        else:
            self.gulliver     = None
            logging.log_debug("omitting marginalization on grid points",unit=self.name)
            
    def Geometry(self,frame):
        """How to process a geometry frame. """
        g=frame["I3Geometry"]
        if self.gulliver:
            self.gulliver.SetGeometry(g)
        else:
            self.logl_service.SetGeometry(g)
        self.PushFrame(frame)
                
    def Physics(self,frame):
        """How to process a Physics frame. (Main Algorithm).
        
        Basic steps of the algorirthm:
        (1) Compute a grid of points surrounding the central position of
        the reconstructed direction. Use the same code or at least the
        same algorithm as in the paraboloid implementaiton.
        (2) Carry out a likelihood computation for these grid-points.
        (3) Interpolate the grid over a given range using a smoothing
        spline based on radial basis functions provided by the scipy
        module.
        (4) Compute likelihood-ratios (-2 * (llh - llh_Min)) for all
        points of the interpolation grid.
        (5) Translate the likelihood-ratios into PValues using a Lookup-
        table based on Kernel-Density-Estimates of the distributions.
        (6) Compute the PValue of given ratio and link it to an area.
        (7) Store these informations in the frame.
        """
        
        
        #ToDo: Alter this code.
        #use 'Put' to add data to the frame.
        nseeds = self.seed_service.SetEvent(frame)
        if nseeds<1:
            return
        self.seed = self.seed_service.GetSeed(0) #Can I Use this?
        self.make_grid() #Possibly alter the grid method? or Use this one?
        self.logl_service.SetEvent(frame) #What does this method do?
        self.neglogl_values = [] #???
        if self.gulliver:
            self.gulliver.SetEvent(frame)
        else:
            self.logl_service.SetEvent(frame)
        for g in self.grid:
            if self.gulliver:
                fitparams = self.gulliver.Fit(g.hypothesis)
                g.value = fitparams.logl
            else:
                g.value = -self.logl_service.GetLogLikelihood(g.hypothesis)
        logging.log_debug("\n".join("%s %f" % (g.hypothesis.particle.dir,g.value) for g in self.grid),unit=self.name)
        axyt   = np.transpose([g.gridproj for g in self.grid])
        alogl  = np.array([g.value for g in self.grid])
        logging.log_debug("axyt=%s" % axyt)
        logging.log_debug("alogl=%s" % alogl)
        fitpar = fit_paraboloid(axyt,alogl,self.seed.particle.dir,self.name)
        frame.Put(self.name+"FitParams",fitpar)
        self.PushFrame(frame)

    def _make_grid(self):
        """Create a grid of points for the evaluation. """
        ################################################################
        #TODO: Add gridpoints to the routine. It might be necessary to
        #alter this code according to the requirements of the new module.
        ################################################################
        
        # define cartesian frame w.r.t. seed direction
        dir0   = self.seed.particle.dir
        dirZEN = I3Direction(dir0.zenith+90.*I3Units.degree,dir0.azimuth)
        dirAZI = dir0.cross(dirZEN)
        # central grid point
        self.grid = [gridpoint(self.seed_service.GetCopy(self.seed),(0.,0.))]
        # self.xygrid = [(0,0)]
        # phi is the angular coordinate on the rings around the seed direction
        dphi=360*I3Units.degree/self.npoints_per_ring
        for i in range(1,self.nrings+1):
            r=i*self.radius/self.nrings
            cosr=cos(r)
            sinr=sin(r)
            for j in range(self.npoints_per_ring):
                sinphi = sin(j*dphi)
                cosphi = cos(j*dphi)
                perp   = cosphi*dirZEN+sinphi*dirAZI # perp is an I3Position object
                gdir   = cosr*dir0+sinr*perp
                seedcp = self.seed_service.GetCopy(self.seed)
                seedcp.particle.dir = I3Direction(*gdir)
                gridproj = (sinr*cosphi, sinr*sinphi)
                self.grid.append(gridpoint(seedcp,gridproj))
                
                

